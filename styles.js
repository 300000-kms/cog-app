import {Style, Stroke, Fill, Circle, Text} from 'ol/style';



var canvas = document.createElement('canvas');
var context = canvas.getContext('2d');

function clamp(num, min, max) {
	return num <= min ? min : num >= max ? max : num;
}

function pattern_line(p) {
	int = 4;
	w = p.w * 2;
	canvas.width = int * p.pixelRatio;
	canvas.height = int * p.pixelRatio;
	context.fillStyle = 'transparent';
	context.fillRect(0, 0, canvas.width, canvas.height);
	context.strokeStyle = p.color;

	context.beginPath();
	context.moveTo(canvas.height, 0);
	context.lineTo(0, canvas.width);
	context.lineWidth = w;
	context.stroke();

	context.beginPath();
	context.moveTo(canvas.height * 2, 0);
	context.lineTo(0, canvas.width * 2);
	context.lineWidth = w;
	context.stroke();

	context.beginPath();
	context.moveTo(canvas.height, -canvas.width);
	context.lineTo(-canvas.height, canvas.width);

	context.lineWidth = w;
	context.stroke();

	pattern = context.createPattern(canvas, 'repeat');
	return pattern
}



function pattern_cross(p) {
	canvas.width = 3 * p.pixelRatio;
	canvas.height = 3 * p.pixelRatio;
	context.fillStyle = 'transparent';
	context.fillRect(0, 0, canvas.width, canvas.height);
	context.fillStyle = 'rgb(30, 30, 30)';
	context.beginPath();
	context.moveTo(0, 0);
	context.lineTo(0, 2 * p.pixelRatio);
	context.lineTo(2 * p.pixelRatio, 2 * p.pixelRatio);
	context.lineWidth = 0.5 * p.w;
	context.stroke();
	pattern = context.createPattern(canvas, 'repeat');
	return pattern
}


function pattern_dot(p) {
	canvas.width = 8 * p.pixelRatio;
	canvas.height = 8 * p.pixelRatio;
	context.fillStyle = 'transparent';
	context.fillRect(0, 0, canvas.width, canvas.height);
	context.fillStyle = 'rgb(30, 30, 30)';
	context.beginPath();
	context.arc(0.5 * p.pixelRatio, 0.5 * p.pixelRatio, 1 * p.pixelRatio, 0, 2 * Math.PI);
	context.fill();
	pattern = context.createPattern(canvas, 'repeat');
	return pattern
}


function st_pattern(zi, str, sca) {
	return new Style({
		zIndex: zi,
		fill: new Fill({
			color: sca
		}),
		stroke: str
	})
}


/* var ast = function (f, r) {
	style = new Style({
		fill: new Fill({
			color: 'rgba(253, 28, 141, 0.56)'
		}),
	});

	style = st_pattern(0, null, pattern_line({
		"pixelRatio": 1.0,
		"w": 0.5,
		color: "rgba(253, 28, 141,1)"

	}))
	return style; 
}*/


export var st2 = function (f, r, z) {

	let fosc = '#000000',
		c_grass = [200,200,200,0.2],
		l = f.get('layer'),
		c = f.get('class'),
		style = null;

	if (l == 'place'){

		style = new Style({
			text: new Text({
				font: '14px sans-serif',
				fill: new Fill({ color: '#000' }),
				stroke: new Stroke({
				  color: '#fff', 
				  width: 5,
				  outlineWidth:4,
				  outline:'#ffffff'
				}),
				text: f.properties_.name
			}),
		  })
	  

	}else if(c == 'motorway' && l == 'transportation') {
		style = new Style({
			zIndex: 9999,
			stroke: new Stroke({
				color: fosc,
				//width: Math.max(10 / r, 4),
				width: 10 / r,
			}),
		})
	} else if (c == 'primary' && l == 'transportation') {
		style = new Style({
			zIndex: 9999,
			stroke: new Stroke({
				color: fosc,
				//width: Math.max(10 / r, 3),
				width: 10 / r,
			}),
		})
	} else if (c == 'secondary' && l == 'transportation') {
		style = new Style({
			zIndex: 9999,
			stroke: new Stroke({
				color: fosc,
				width: 10 / r,
				//lineDash: [.5, 5]
			}),
		})
	} else if (c == 'tertiary' && l == 'transportation') {

		style = new Style({
			zIndex: 9999,
			stroke: new Stroke({
				color: fosc,
				width: 7 / r,
				//lineDash: [.5, 5]
			}),
		})

	} else if (c == 'minor' && l == 'transportation' && r < 7) {

		style = new Style({
			zIndex: 500,
			stroke: new Stroke({
				color: fosc,
				width: 7 / r,
				//lineDash: [.5, 5]
			}),
		})

	} else if (c == 'path' && l == 'transportation' && r < 7) {

		style = new Style({
			zIndex: 300,
			stroke: new Stroke({
				color: [0, 0, 0, 0.5],
				width: 7 / r,
				//lineDash: [.5, 5]
			}),
		})

	} else if (c == 'service' && l == 'transportation' && r < 7) {
		style = new Style({
			zIndex: 200,
			stroke: new Stroke({
				color: [0, 0, 0, 0.5],
				width: 7 / r,
				//lineDash: [.5, 5]
			}),
		})
	} else if (l == 'transportation') {
		style = null
		//		style = new Style({
		//			zIndex: 9999,
		//			stroke: new Stroke({
		//				color: [0, 0, 0, 0.5],
		//				width: 0.1,
		//				//lineDash: [.5, 5]
		//			}),
		//		})

	} else if (c == 'grass' && l == 'landcover') {
		style = new Style({
			fill: new Fill({
				color: c_grass
			}),
		})
	} else if (c == 'wood' && l == 'landcover') {

		style = new Style({
			fill: new Fill({
				color: c_grass
			}),
		})
	} else if (c == 'national_park' && l == 'park') {
		style = new Style({
			fill: new Fill({
				color: c_grass
			}),
		})
	} else if (c == 'nature_reserve' && l == 'park') {

		style = new Style({
			fill: new Fill({
				color: c_grass
			}),
		})
	} else if (c == 'ocean' && l == 'water') {

		style = new Style({

			stroke: new Stroke({
				color: fosc,
				width: 1,
			}),

			fill: new Fill({
				color: '#ffffff'
			}),
		})

	} else if (l == 'building' && r < 1) {

		style = new Style({

			stroke: new Stroke({
				color: [0, 0, 0, 0.5],
				width: 1,
			}),
		})

	} else {


	}

	return style;
}