import './style.css';

import {Map, View, Overlay} from 'ol';
import {WebGLTile as WebGLTileLayer, Tile as TileLayer, Vector as VectorLayer, Group} from 'ol/layer';
import VectorTileLayer from 'ol/layer/VectorTile';
import VectorTileSource from 'ol/source/VectorTile';
import {createXYZ} from 'ol/tilegrid';
import MVT from 'ol/format/MVT';
import {GeoTIFF, OSM, Vector as VectorSource} from 'ol/source';
import {fromLonLat, toLonLat, transformExtent} from 'ol/proj';
import {Control, ScaleLine, defaults as defaultControls, Attribution} from 'ol/control';
import GeoJSON from 'ol/format/GeoJSON';
import {Style, Stroke, Fill, Circle} from 'ol/style';
import Feature from 'ol/Feature';
import {Point, Polygon} from 'ol/geom';
 
import {st2} from './styles.js';

const //years = ["vars", 2015, 2000, 1990, 1975],
      years = ["vars", "abs"],
      titles = ['variation on urbanisation and demography <br> between 1975-2015',
                'urbanistaion an demography <br> in 2015'],
      apiUrl = 'https://uia.300000.eu/?m=city_data&a=',
      baseUrl = 'https://cog.300000.eu/',
      cogsUrb = [
        baseUrl + 'var_built_2015_1975_cog',
        baseUrl + 'GHS_BUILT_LDS2014_GLOBE_R2018A_54009_250_V2_0_cog',
        // baseUrl + 'GHS_BUILT_LDS2000_GLOBE_R2018A_54009_250_V2_0_cog',
        // baseUrl + 'GHS_BUILT_LDS1990_GLOBE_R2018A_54009_250_V2_0_cog',
        // baseUrl + 'GHS_BUILT_LDS1975_GLOBE_R2018A_54009_250_V2_0_cog',
      ],
      cogsPob = [
        baseUrl + 'var_pop_2015_1975_cog',
        baseUrl + 'GHS_POP_E2015_GLOBE_R2019A_54009_250_V1_0_cog',
        // baseUrl + 'GHS_POP_E2000_GLOBE_R2019A_54009_250_V1_0_cog',
        // baseUrl + 'GHS_POP_E1990_GLOBE_R2019A_54009_250_V1_0_cog',
        // baseUrl + 'GHS_POP_E1975_GLOBE_R2019A_54009_250_V1_0_cog',
      ];

let selectedYear = "vars",
    map,
    initTimer,
    cogLayersUrb = {},
    cogLayersPob = {},
    cogLayersUrbSourceInfo = {},
    cogLayersPobSourceInfo = {},
    cogLayersUrbLoaded = false,
    cogLayersPobLoaded = false,
    minUrb = Infinity,
    maxUrb = -Infinity,
    minPob = Infinity,
    maxPob = -Infinity;

/*
 * 1. Get bounds from UIA API
 ******************************/
const cityId = new URL(location.href).searchParams.get('id');
if (cityId && cityId !== undefined && Number.isInteger(parseInt(cityId))) {

  console.log("load city:", cityId);

  // get info from city by cityId
  fetch(apiUrl+cityId)
    .then(response => response.json())
    .then(data => {

      if (data.message) {
        console.log(data.message.uc_nm_mn, "/", data.message.xc_nm_lst);

        let  el = document.getElementById('city');
        el.innerHTML = data.message.uc_nm_mn;

        let center = fromLonLat([
            parseFloat(data.message.gcpnt_lon),
            parseFloat(data.message.gcpnt_lat)
          ],
          'EPSG:3857'
        );
        
        /* ajustar el extent en caso de que sea muy pequeño */
        data.message.bbx_lonmn = parseFloat(data.message.bbx_lonmn);
        data.message.bbx_latmn = parseFloat(data.message.bbx_latmn);
        data.message.bbx_lonmx = parseFloat(data.message.bbx_lonmx);
        data.message.bbx_latmx = parseFloat(data.message.bbx_latmx);

        let mylon = 0.5, mylat= 0.5;

        if ( (data.message.bbx_lonmx-data.message.bbx_lonmn) < mylon){
          let lonmn = data.message.bbx_lonmn,
              lonmx = data.message.bbx_lonmx;

          data.message.bbx_lonmn = lonmn-((mylon-(lonmx-lonmn))/2);
          data.message.bbx_lonmx = lonmx+((mylon-(lonmx-lonmn))/2);
        }

        if ( (data.message.bbx_latmx-data.message.bbx_latmn) < mylat){

          let latmn = data.message.bbx_latmn,
              latmx = data.message.bbx_latmx;
          data.message.bbx_latmn = latmn-((mylat-(latmx-latmn))/2);
          data.message.bbx_latmx = latmx+((mylat-(latmx-latmn))/2);
        }

        let extent = transformExtent([
            data.message.bbx_lonmn,
            data.message.bbx_latmn,
            data.message.bbx_lonmx,
            data.message.bbx_latmx
          ],
          'EPSG:4326',
          'EPSG:3857'
        );

        //console.log(center, extent);

        createMap(center, extent);
        addVectorLayer();
        loadCogs();
        addGridLayer();
      }
      else {
        //clearInterval(timer);
        throw new Error("city "+cityId+" does not exist, try with ?id=Integer:");
      }
    });
}
else {
  //clearInterval(timer);
  throw new Error("city "+cityId+" does not exist, try with ?id=Integer:");
}

/*
 * 2. Create Openlayers map
 ******************************/
function createMap(center, extent) {
  map = new Map({
    target: 'map',
    layers: [],
    view: new View(getViewObj(center, extent)),
    controls: defaultControls().extend([new ScaleLine()]),
    selectedYear: selectedYear
  });

  // start spinner
  map.getTargetElement().classList.add('spinner');
  initTimer = setTimeout(function() {
    console.log("seguro");
    loadYear(selectedYear);
  }, 7000);
}

function getViewObj(center, extent) {
  let res0 = 611.4962262814100313;
  let viewObj = {
    resolutions: [
      res0*16,
      res0*8,
      res0*4,
      res0*2,
      res0, // zoom 4
      res0/2,
      res0/4,
      res0/8,
      res0/16,
      res0/32,
      res0/64, // zoom 10
      res0/128,
    ],
    center: center,
    extent: extent,
    zoom: 6,
    minZoom: 6,
    maxZoom: 9
  }

  return viewObj;
}

/*
 * 3. Load OSM vector layer
 ******************************/
function addVectorLayer() {
  let osmVectorLayer= new VectorTileLayer({
    //renderMode: 'image',
    //tilePixelRatio: 16,
    //maxResolution: 0.2,
    
    source: new VectorTileSource({
      className: 'multiply',
      type: "base",
      title: 'osm_vector',
      format: new MVT({}),
       tileGrid: new createXYZ({
          minZoom: 1,
          maxZoom: 11
      }), 
      url: 'https://tileserver{0-5}.300000.eu/osm_low/{z}/{x}/{y}.pbf'
    }),
  });

  osmVectorLayer.setZIndex(999);

  let currentZoom = null;
  let mystyles = {};

  function osm_style(f,r) {
    let z = map.getView().getZoom();
    let l = f.get('layer');
    let c = f.get('class');

    if (z != currentZoom) {
      mystyles = {};
      currentZoom = z;
    }

    if (l == 'place' && f.get('rank') < 12) {
      return st2(f,r,z);
    }
    else if (l != 'place') {
      if (mystyles[l+'_'+c] != undefined){
        return mystyles[l+'_'+c] ;
      }
      else {
        mystyles[l+'_'+c] = st2(f,r,z);
        return mystyles[l+'_'+c] 
      }
    }
  }

  osmVectorLayer.setStyle(osm_style);

  map.addLayer(osmVectorLayer);
}

/*
 * 4. Load COG geotiffs and related jsons
 ******************************/
function loadCogs() {
  // load all geoTiffs
  for (let i=0; i<years.length; i++) {
    cogLayersUrb[years[i]] = loadLayer(cogsUrb[i]);
    cogLayersPob[years[i]] = loadLayer(cogsPob[i]);

    loadLayerSourceInfo("urb", i, cogsUrb[i]);
    loadLayerSourceInfo("pob", i, cogsPob[i]);

    //loadLayerJson("urb", i, cogsUrb[i]);
    //loadLayerJson("pob", i, cogsPob[i]);
  }

  // initial load
  cogLayersPob[selectedYear].getSource().getView().then(() => {
    loadYear(selectedYear);
  });

  /*cogLayersPob[selectedYear].on('loadend', function() {
    console.log("loadend");
  });
  cogLayersUrb[selectedYear].on('loadend', function() {
    console.log("loadend");
  });*/
}

function loadLayer(url) {
  return new WebGLTileLayer({
    opacity: 0,
    zIndex: 0,
    source: new GeoTIFF({
      sources: [
        {
          url: url + ".tif",
          max: 255,
          crossOrigin: "anonymous"
        },
      ],
      interpolate: false,
      normalize: true
    })
  });
}

function loadLayerJson(type, i, url) {
  fetch(url + "_stats.json")
    .then(response => response.json())
    .then(data => {

      if (data) {
        //console.log(data.bands[0].minimum, data.bands[0].maximum);

        let cogLayer = new WebGLTileLayer({
          opacity: 0,
          zIndex: 0,
          source: new GeoTIFF({
            sources: [
              {
                url: url + ".tif",
                min: data.bands[0].minimum,
                max: data.bands[0].maximum,
                crossOrigin: "anonymous"
              },
            ],
            interpolate: false,
            normalize: true
          })
        });

        if (type === "urb") cogLayersUrb[years[i]] = cogLayer;
        else if (type === "pob") cogLayersPob[years[i]] = cogLayer;
      }
      else {
        console.log(url+"_stats.json", "does not exist");
      }
    });
}

function loadLayerSourceInfo(type, i, url) {
  fetch(url + "_stats.json")
    .then(response => response.json())
    .then(data => {

      if (data) {
        //console.log(data.bands[0].minimum, data.bands[0].maximum);

        let sourceInfo = {
          min: data.bands[0].minimum,
          max: data.bands[0].maximum,
        }

        if (type === "urb") cogLayersUrbSourceInfo[years[i]] = sourceInfo;
        else if (type === "pob") cogLayersPobSourceInfo[years[i]] = sourceInfo;
      }
      else {
        console.log(url+"_stats.json", "does not exist");
      }
    });
}

function loadYear(year, oldYear = null) {
  console.log(year, oldYear);

  selectedYear = year;

  if (oldYear) {
    map.removeLayer(cogLayersUrb[oldYear]);
    map.removeLayer(cogLayersPob[oldYear]);
  }

  map.addLayer(cogLayersUrb[selectedYear]);
  map.addLayer(cogLayersPob[selectedYear]);

  updateDataGrid();
}

/*
 * 5. Make Grid from geotiffs
 ******************************/
let maxRadius = 10,   // from 1 to 10
    urbStyles = new Array(maxRadius),
    pobStyles = new Array(maxRadius),
    color_1=[227, 33, 34,  0.8],
    color_2=[90, 145, 200, 1];

for (let i=0; i<maxRadius; i++) {
  
  // turquesa
  urbStyles[i] = new Style({
    image: new Circle({
      fill: new Fill({
        color: color_1
      }),
      stroke: new Stroke({
        color: color_1
      }),
      radius: i+1
    })
  });

  // morado
  pobStyles[i] = new Style({
    image: new Circle({
      fill: new Fill({
        color: color_2
      }),
      stroke: new Stroke({
        color: color_2
      }),
      radius: i+1
    })
  });
}

let gridSourceUrb = new VectorSource();
const gridLayerUrb = new VectorLayer({
  name: 'layerUrb',
  source: gridSourceUrb,
  zIndex: 1,
  //className: 'multiply',
   style: function(feature) {
    let val = feature.get("valueUrb") * 255/(maxUrb-minUrb),
        r = Math.round(val*(maxRadius-1)),
        opacity = feature.get("opacityUrb");

    if (r > 0 && opacity > 0)
      return urbStyles[r];
    else
      return null;
  }
});

const gridLayerPob = new VectorLayer({
  name: 'layerPob',
  source: gridSourceUrb,
  zIndex: 1,
  //className: 'multiply',
   style: function(feature) {
    let val = feature.get("valuePob") * 255/(maxPob-minPob),
        r = Math.round(val*(maxRadius-1)),
        opacity = feature.get("opacityPob");

    if (r > 0 && opacity > 0)
      return pobStyles[r];
    else
      return null;
  }
});

let gridGroup = new Group({
  title: selectedYear,
  layers: [
    gridLayerPob,
    gridLayerUrb
  ]
});

function addGridLayer() {
  map.addLayer(gridGroup);
  gridLayerPob.on("postrender", function(evt) { evt.context.globalCompositeOperation = "multiply"; });
  gridLayerUrb.on("postrender", function(evt) { evt.context.globalCompositeOperation = "multiply"; });
 
  map.getView().on('change:center', function() {
    updateDataGrid();
  });

  // add yearControl
  for (let i=0; i<years.length; i++) {
    let yearControl = new YearControl({
      year:  years[i],
      title: titles[i],
      gridGroup: gridGroup,
      active: years[i] === selectedYear
    })
    map.addControl(yearControl);
  }

  // Marker popup
  let overlayPopup = new Overlay({
    element: document.getElementById('popup')
  });
  map.addOverlay(overlayPopup);

  map.getView().on('change:resolution', function() {
    overlayPopup.setPosition(undefined);
    updateDataGrid();
  });

  map.on('pointermove', function(evt) {
    overlayPopup.setPosition(undefined);

    if (evt.dragging) {
      return;
    }

    let pixel = map.getPixelFromCoordinate(evt.coordinate),
        output = '',
        dataUrb = cogLayersUrb[selectedYear].getData(evt.pixel),
        dataPob = cogLayersPob[selectedYear].getData(evt.pixel);

    if (dataUrb && dataPob) {
      if (dataUrb[0] !== 0 || dataPob[0] !== 0) {
        dataPob = dataPob[0]/255*(cogLayersPobSourceInfo[selectedYear].max-cogLayersPobSourceInfo[selectedYear].min);
        dataUrb = dataUrb[0]/255*(cogLayersUrbSourceInfo[selectedYear].max-cogLayersUrbSourceInfo[selectedYear].min);
        output += '<p><span class="dot_pob"></span>Population: '+parseInt(dataPob)+'</p>';
        output += '<p><span class="dot_urb"></span>Urbanization: '+parseInt(dataUrb)+'</p>';

        document.getElementById('popup-content').innerHTML = output;
        overlayPopup.setPosition(evt.coordinate);
      }
    }
  });

  map.on('click', function(evt) {
    console.log(cogLayersUrb[selectedYear].getData(evt.pixel)[0],cogLayersUrbSourceInfo[selectedYear].max,cogLayersUrbSourceInfo[selectedYear].min);
    console.log(cogLayersPob[selectedYear].getData(evt.pixel)[0],cogLayersPobSourceInfo[selectedYear].max,cogLayersPobSourceInfo[selectedYear].min);
    let dataUrb = cogLayersUrb[selectedYear].getData(evt.pixel)[0]/255*(cogLayersUrbSourceInfo[selectedYear].max-cogLayersUrbSourceInfo[selectedYear].min);
    let dataPob = cogLayersPob[selectedYear].getData(evt.pixel)[0]/255*(cogLayersPobSourceInfo[selectedYear].max-cogLayersPobSourceInfo[selectedYear].min);
    console.log(dataUrb, dataPob, evt.coordinate);
  });

  let reloadControl = new ReloadControl({})
  map.addControl(reloadControl);
}

/*
 * 6. Update data grid
 ******************************/
function updateDataGrid() {
  console.log("getData", map.getView().getZoom());

  let extent = map.getSize(),
      gridSize = 22,
      features = [],
      distriVal = [],
      distriOpa = [];
      //sourceInfoUrb = cogLayersUrb[selectedYear].getSource().sourceInfo_[0],
      //sourceInfoPob = cogLayersPob[selectedYear].getSource().sourceInfo_[0];

  // geotiff metadata like min and max if saved
  //console.log(cogLayersUrb[selectedYear].getSource().getMetadata(), cogLayersPob[selectedYear].getSource().getMetadata());

  // scan geotiff
  for (let x = gridSize/2; x<extent[0]; x+=gridSize) {
    for (let y = gridSize/2; y<extent[1]; y+=gridSize) {

      let valUrb = cogLayersUrb[selectedYear].getData([x,y]),
          valPob = cogLayersPob[selectedYear].getData([x,y]);

      if (valUrb && valPob) {
        // normalize geotiff 2 band: [value, opacity]
        features.push(new Feature({
          geometry: new Point(map.getCoordinateFromPixel([x,y])),
          //valueUrb: (valUrb[0]+sourceInfoUrb.min)/sourceInfoUrb.max,
          //valuePob: (valPob[0]+sourceInfoPob.min)/sourceInfoPob.max,
          valueUrb: valUrb[0]/255,
          valuePob: valPob[0]/255,
          opacityUrb: valUrb[1]/255,
          opacityPob: valPob[1]/255
        }));

        if (valUrb[0] < minUrb) minUrb = valUrb[0];
        else if (valUrb[0] > maxUrb) maxUrb = valUrb[0];

        if (valPob[0] < minPob) minPob = valPob[0];
        else if (valPob[0] > maxPob) maxPob = valPob[0];
      }
    }
  }
  //console.log("points:", features);

  if (features.length > 0) {
    // add grid layer
    gridSourceUrb.clear();
    gridSourceUrb.addFeatures(features);

    // stop spinner
    map.getTargetElement().classList.remove('spinner');
    clearTimeout(initTimer);
  }
  else {
    console.log("again");
    setTimeout(function() {
      updateDataGrid();
    }, 1000);
  }
}

/*
 * Button control to activate year layers
 *****************************************/
class YearControl extends Control {
  /**
   * @param {Object} [opt_options] Control options.
   */
  constructor(opt_options) {
    const options = opt_options || {};

    const button = document.createElement('button');
    button.innerHTML = options.title;
    button.classList.add('selector');

    let active = "";
    if (options.active)
      active = ' ol-active';

    const element = document.createElement('div');
    element.className = 'year-control ol-unselectable ol-control year-control-'+options.year+active;
    
    element.appendChild(button);

    super({
      element: element,
      target: options.target
    });

    this.year = options.year;
    //this.gridGroup = options.gridGroup;

    button.addEventListener('click', this.handleYearControl.bind(this), false);
  }

  handleYearControl() {
    // start spinner
    map.getTargetElement().classList.add('spinner');

    // select control
    let elements = document.getElementsByClassName('year-control');
    for (let i=0; i < elements.length; i++) {
      elements[i].classList.remove('ol-active');;
    }
    document.getElementsByClassName('year-control-'+this.year)[0].classList.add('ol-active');

    loadYear(this.year, selectedYear);
  }
}

/*
 * Button control a complete reload
 *****************************************/
class ReloadControl extends Control {
  /**
   * @param {Object} [opt_options] Control options.
   */
  constructor(opt_options) {
    const options = opt_options || {};

    const button = document.createElement('button');
    button.innerHTML = '&#8635;';

    const element = document.createElement('div');
    element.className = 'reload-control ol-unselectable ol-control';
    
    element.appendChild(button);

    super({
      element: element,
      target: options.target
    });

    button.addEventListener('click', this.handleReloadControl.bind(this), false);
  }

  handleReloadControl() {
    window.location.reload();
  }
}